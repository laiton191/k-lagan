package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author John Gonzalez
 */
@Data
@Entity
@Table(name = "Prices")
public class Prices implements Serializable {

    @Column(name = "BRAND_ID")
    private int BRAND_ID;
    @Column(name = "START_DATE")
    private Date START_DATE;
    @Column(name = "END_DATE")
    private Date END_DATE;
    @Column(name = "PRICE_LIST")
    private int PRICE_LIST;
    @Column(name = "PRODUCT_ID")
    private int PRODUCT_ID;
    @Column(name="PRIORITY")
    private int PRIORITY;
    @Column(name = "PRICE")
    private long PRICE;
    @Column(name = "CUR")
    private int CUR;

}

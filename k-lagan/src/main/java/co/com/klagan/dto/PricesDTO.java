
package co.com.klagan.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
/**
 *
 * @author John Gonzalez
 */
@Data
public class PricesDTO implements Serializable{
    
    private int product_id;
    private int brand_id;
    private int price_list;
    private Date start_date;
    private long price;
    
}

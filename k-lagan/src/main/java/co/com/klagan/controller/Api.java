/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.klagan.controller;

import co.com.klagan.dto.PricesDTO;
import interfaces.IPricesServices;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author John Gonzalez
 */
@RestController
@RequestMapping("api")
public class Api {

    @Autowired
    IPricesServices iPricesServices;
    
    @GetMapping("/klagan")
    public Optional<PricesDTO> findByKlagan(Date starDate, int productID, int brandId){
        return iPricesServices.findByKlagan(starDate, productID, brandId);
    }

}

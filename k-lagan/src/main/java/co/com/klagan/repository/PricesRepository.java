
package co.com.klagan.repository;

import entities.Prices;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author John Gonzalez
 */
@Repository
public interface PricesRepository extends CrudRepository<Prices, Integer>{
    
    @Transactional(readOnly = true)
    Optional<Prices> findBySTART_DATEAndPRODUCT_IDAndBRAND_ID(Date starDate, int productID, int brandId);
}

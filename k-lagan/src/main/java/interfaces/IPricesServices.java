
package interfaces;

import co.com.klagan.dto.PricesDTO;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 *
 * @author John Gonzalez
 */
@Service
public interface IPricesServices {
    List<PricesDTO> findAll();
    
    Optional<PricesDTO> findByKlagan(Date starDate, int productID, int brandId);
}
